**بسم اللّه الرحمن الرحيم**
> 
> Basic Algorithms Explaination


This file is mainly dedicated to emphasize and summarize basic programming algorithms, with a brief explaination of each
algorithms and how to use it.

Algorithms that had been covered:

- ⌠• Merge Sort Algorithm.

- ⌠• Selection Sort Algorithms.

- ⌠• Bubble Sorting Algorithms.

- ⌠• Insertion Sorting Algorithms.




![alt text](https://i.pinimg.com/564x/30/4e/82/304e82e007ec69b55c268315c3bf93d2.jpg)
